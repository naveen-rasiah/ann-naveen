import datetime
from pymongo import MongoClient
from pprint import pprint

client = MongoClient('localhost', 27017)

db = client.wedding

# rsvp
rsvp_init_doc = {
	'session_stamp': 'aBc1',
	'guests': [
		{
			'user_id': '',
			'attedance': True
		}
	],
	'email': '',
	'comment': '',
	'ip': '',
	'leader': '',
}

coll_rsvp = db.rsvp
result = coll_rsvp.insert_one(rsvp_init_doc)

for doc in coll_rsvp.find({}):
	pprint(doc)

# people
user_init_doc = {
	'name': '',
	'nickname': '',
	'password': '',
	'leader': '',
	'email': ''
}

coll_user = db.user
result = coll_user.insert_one(user_init_doc)

for doc in coll_user.find({}):
	pprint(doc)
