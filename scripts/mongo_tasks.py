from pprint import pprint
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime

client = MongoClient('localhost', 27017)
db = client.wedding

def add(coll, **docs):
	coll.insert_one(docs)
	print("Added: ")
	pprint(docs)


def delete(coll, **docs):
	result = coll.remove(docs)
	print("Removed: ")
	pprint(docs)

def delete_by_id(coll, _id):
	delete(
		coll, 
		**{'_id': ObjectId(_id)}
	)

def delete_all(coll):
	delete(coll, **{})


def delete_rsvp(_id):
	delete_by_id(db.rsvp, _id)

def delete_user(_id):
	delete_by_id(db.user, _id)

def find_rsvp_by_ip(ip):
	for rsvp in db.rsvp.find():
		if rsvp['ip'] == ip:
			pprint(rsvp)

def update_leader_name(_id, new_name):
	rsvp = db.rsvp.find({
		'_id': ObjectId(_id)
	})[0]

	leader_id = rsvp['leader']['_id']
	rsvp['leader']['name'] = new_name
	db.rsvp.save(rsvp)

	user = db.user.find({
		'_id': ObjectId(leader_id)
	})[0]

	user['name'] = new_name
	db.user.save(user)

def add_guest_to_rsvp(_id, guest_info):
	rsvp = db.rsvp.find({
		'_id': ObjectId(_id)
	})[0]

	leader_id = rsvp['leader']['_id']
	guest_info['leader_id'] = leader_id
	guest_info['date_added'] = datetime.now()
	guest_info['nickname'] = ''
	guest_info['password'] = ''

	db.user.save(guest_info)
	user = db.user.find(guest_info)[0]

	rsvp['guests'].append(user)
	db.rsvp.save(rsvp)

def _add_guest_to_rsvp(rsvp_id, guests):
	for guest in guests:
		add_guest_to_rsvp(
			rsvp_id,
			{
				'name': guest[0],
				'attendance': guest[1],
				'email': guest[2],
			}
)


def update_comment(_id, comment):
	rsvp = db.rsvp.find({
		'_id': ObjectId(_id)
	})[0]
	rsvp['comments'] = comment
	db.rsvp.save(rsvp)


rsvp_id = '5b0330463837511e58b23318'
user_id = ''
comments = 'Your  wedding day will come and go, but may your love forever grow.'

guests = [
	['Annas Vance', True, '']
]

#update_leader_name(rsvp_id,'Antony')
#_add_guest_to_rsvp(rsvp_id, guests)
delete_rsvp(rsvp_id)
# delete_user(user_id)

#update_comment(rsvp_id, comments)

client.close()
