import requests
import simplejson as json
from pprint import pprint

url = "http://localhost:5000/rsvp/post_rsvp_data"

data = {u'leader': {
			u'attendance': True,
			u'name': u'Naveen Rasiah',
			u'email': u'naveen@test.com'},
		u'comments': u'This is my comment', 
		u'guests': {
			u'guest_2': {
				u'attendance': False, 
				u'name': u'Daffy Duck', 
				u'email': u''}, 
			u'guest_3': {
				u'attendance': True, 
				u'name': u'Scrooge McDuck', 
				u'email': u''}, 
			u'guest_1': {
				u'attendance': True, 
				u'name': u'Bugs Bunny', 
				u'email': u''
		}}}

headers = {'Content-type': 'application/json'}
r = requests.post(url, data=json.dumps(data), headers=headers)
pprint(r.content)