DEBUG = True

# MongoDB Settings
MONGOALCHEMY_PORT = 27017
MONGOALCHEMY_SERVER = 'localhost'
MONGOALCHEMY_DATABASE = 'wedding'

SESSION_TYPE = 'filesystem'