from flask import (Blueprint, 
				   render_template,
				   request)


bridal = Blueprint('bridal',
				   __name__,
				   url_prefix='/bridal',
				   template_folder='templates',
				   static_folder='static')


@bridal.route('/registry')
def registry():
	return render_template('registry.html')