from app import app
from flask import (Blueprint, 
				   render_template,
				   redirect,
				   url_for)

save = Blueprint('save',
				  __name__,
				  url_prefix='/save',
				  template_folder='templates',
				  static_folder='static')

date = Blueprint('date',
				  __name__,
				  url_prefix='/date',
				  template_folder='templates',
				  static_folder='static')


@save.route('/')
def save_the_date():
	return redirect(url_for('bridal.registry'))

@date.route('/')
def save_the_date_alternate():
	print app
	return render_template('save-the-date-walking.html')

@app.route("/")
def hello():
	return render_template('save-the-date-brown.html')
