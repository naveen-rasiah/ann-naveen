import os
import json
import bson

from pprint import pprint
from flask import (Blueprint, 
				   render_template,
				   request,
				   session,
				   redirect)
from flask_api import status
from ..rsvp.models import (user as USER,
						   rsvp as RSVP)
from ..rsvp.views import get_post_data


admin = Blueprint('admin',
				  __name__,
				  url_prefix='/admin',
				  template_folder='templates',
				  static_folder='static')


def get_sp():
	sfile = os.path.join(
		os.path.dirname( __file__ ), 
		'../../secret.txt'
	)
	with open(sfile, 'r') as file:
		for line in file:
			secret = line.strip()

	return secret	


def query_all_from_rsvp():
	results = {}
	for rsvp in RSVP.query.all():
		results[str(rsvp.id())] = {
			'_id': str(rsvp.id()),
			'leader': rsvp.leader.name,
			'guests': [],
			'date': rsvp.date_added,
			'comments': rsvp.comments, 
			'email': rsvp.leader.email,
			'ip': rsvp.ip,
			'guest_of': rsvp.guest_of
		}

		results[str(rsvp.id())]['guests'].append([
			rsvp.leader.name,
			rsvp.leader.attendance
		])

		for guest in rsvp.guests:
			results[str(rsvp.id())]['guests'].append([
				guest.name,
				guest.attendance
			])

	return results


def query_one_rsvp(_id):
	_id = bson.objectid.ObjectId(_id)
	return RSVP.query.filter({RSVP.mongo_id: _id})[0]


@admin.route('/change_guest_of', methods=['POST'])
def change_guest_of():
	rsvp = query_one_rsvp(
			get_post_data()
		 )

	who_map = {'': 0, 'AR': 1, 'AF': 2, 'NR': 3, 'NF': 4}
	who_map_inv = {v: k for k, v in who_map.iteritems()}

	next_option = (who_map[rsvp.guest_of] + 1) % len(who_map)
	rsvp.guest_of = who_map_inv[next_option]
	rsvp.save()

	return json.dumps({'next': rsvp.guest_of}), status.HTTP_200_OK


@admin.route('/check_admin', methods=['POST'])
def check_admin():
	data = get_post_data()
	value =  data['value']
	secret = get_sp()

	session['logged_in'] = False
	if value == secret:
		session['logged_in'] = True
		s200 = status.HTTP_200_OK
		return json.dumps({'url': '/admin/rsvp'}), s200

	return json.dumps({}), status.HTTP_400_BAD_REQUEST


@admin.route('/')
def admin_login():
	return render_template('login.html')


def total_attending_guests():
	rsvp_data = query_all_from_rsvp()
	total_guests = 0
	breakdown = {'': 0, 'AR': 0, 'AF': 0, 'NR': 0, 'NF': 0}
	for rsvp in rsvp_data:
		guests = rsvp_data[rsvp]['guests']
		guest_of = rsvp_data[rsvp]['guest_of']
	 	for guest in guests:
	 		if guest[1]:
	 			total_guests+=1
	 			breakdown[guest_of]+=1

	total_data = {
		'total_guests': total_guests,
		'breakdown': breakdown
	}
	return total_data


@admin.route('/rsvp')
def admin_rsvp():
	try:
		session['logged_in']
	except:
		return redirect('admin')

	if not session['logged_in']:
		return redirect('admin')

	rsvp_data = query_all_from_rsvp()
	total_data = total_attending_guests()
	total_guests = total_data['total_guests']
	breakdown = total_data['breakdown'] 

	return render_template('admin.html',
						   data=rsvp_data,
						   total_guests=total_guests,
						   guests_breakdown=breakdown
						   )