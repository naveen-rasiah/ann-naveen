from flask import (Blueprint, 
				   render_template)

invite = Blueprint('invite',
				   __name__,
				   url_prefix='/invite',
				   template_folder='templates',
				   static_folder='static')


@invite.route('/')
def invite_home():
	return render_template('invite.html')
