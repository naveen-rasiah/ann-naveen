import json
import bson

from pprint import pprint
from flask import (Blueprint, 
				   render_template,
				   request)
from models import (user as USER,
					rsvp as RSVP)


rsvp = Blueprint('rsvp',
				  __name__,
				  url_prefix='/rsvp',
				  template_folder='templates',
				  static_folder='static')


def bson_check(value):
	return bson.objectid.ObjectId.is_valid(value)


def get_post_data():
	if request.method:
		if request.method == "POST":
			return request.get_json()


def scrape_bson_objects_from_results(tmp):
	results = {}
	for key, value in tmp.iteritems():
		if isinstance(value, dict):
			for k, v in value.iteritems():
				if not bson_check(v):
					results.setdefault(key, {})
					results[key][k] = v
		elif isinstance(value, list):
			results.setdefault(key, [])
			for x in range(len(value)):
				results[key].append({})
				for k, v in value[x].iteritems():
					if not bson_check(v):
						results[key][x].setdefault(k, {})
						results[key][x][k] = v
		else:
			if not bson_check(value):
				results[key] = value

	return results


def convert_query_to_json(query):
	result = []
	for b in query:
		result.append(b.wrap())
	return result


@rsvp.route('/')
def rsvp_home():
	return render_template('rsvp.html')


@rsvp.route('/post_rsvp_data', methods=['POST'])
def post_rsvp_data():
	data = get_post_data()

	leader = data["leader"]
	guests = data["guests"]
	comments = data["comments"]

	_leader = USER(name=leader["name"],
				   nickname="",
				   password="",
				   email=leader["email"],
				   attendance=leader["attendance"])
	_leader.set_self_as_leader_and_save()

	_rsvp = RSVP(leader=_leader,
				 guests=[],
				 comments=comments,
				 ip=request.remote_addr)
	_rsvp.save()

	for key, value in guests.iteritems():
		_guest = USER(name=guests[key]["name"],
					  nickname="",
					  password="",
					  email="",
					  attendance=guests[key]["attendance"])
		_guest.set_leader_and_save(_leader.id())
		_rsvp.add_user_and_save(_guest)

	results = scrape_bson_objects_from_results(
				convert_query_to_json(
					RSVP.query.filter(RSVP.mongo_id == _rsvp.id())
				)[0]
			)

	return json.dumps({'status':'OK'})