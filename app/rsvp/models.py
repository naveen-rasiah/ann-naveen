import datetime
from app import db

class user(db.Document):
	name = db.StringField()
	nickname  = db.StringField()
	password = db.StringField()
	leader_id = db.ObjectIdField(required=False)
	email  = db.StringField()
	attendance = db.BoolField()
	date_added = db.DateTimeField(default=datetime.datetime.now())

	def id(self):
		return self.mongo_id

	def set_self_as_leader_and_save(self):
		self.save()
		self.leader_id = self.mongo_id
		self.save()

	def set_leader_and_save(self, leader_id):
		self.save()
		self.leader_id = leader_id
		self.save()


class rsvp(db.Document):
	leader = db.DocumentField(user)
	guests = db.ListField(db.DocumentField(user))
	comments = db.StringField()
	ip = db.StringField()
	date_added = db.DateTimeField(default=datetime.datetime.now())
	guest_of = db.StringField(default='')

	def add_user_and_save(self, _user):
		self.guests.append(_user)
		self.save()

	def id(self):
		return self.mongo_id