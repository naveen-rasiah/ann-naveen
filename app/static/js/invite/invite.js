var data = {};


// Helper Functions

function call_back_function(function_to_call, params){
	function_to_call(params)
}


function post_data(data, 
				   function_to_call_on_success=null){
	$.ajax({
		type: 'POST',
		url: '/rsvp/post_rsvp_data',
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify(data),
		success: function(response) {

		},
		error: function(error) {}
	});
}



function add_class_to_element(element_id, css_class){
	$('#'+element_id).addClass(css_class)
		
}


function remove_class_from_element(element_id, css_class){
	$('#'+element_id).removeClass(css_class)
}


// Invite Functions
function initalize(){
  $('#slick-div').slick({
  	arrows: true
  });
}


jQuery(document).ready(function() {
    jQuery(document).ready(function() {
    	initalize()
    });
}) 