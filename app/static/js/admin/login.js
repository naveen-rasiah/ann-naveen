function try_again(){
	$("#instructions").fadeOut('slow', function(){
		$("#instructions").text("try again")
		$("#instructions").fadeIn("slow")
	})
}

function redirect(url){
	window.location.href = url;
}

function check_and_send(){
	data = {
		value: $('#irakaciya').val()
	}
	post_data(data)
}

// Helper Functions

function call_back_function(function_to_call, params){
	function_to_call(params)
}


function post_data(data, 
				   function_to_call_on_success=null){
	$.ajax({
		type: 'POST',
		url: '/admin/check_admin',
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify(data),
		success: function(response) {
			redirect(response.url)
		},
		error: function(error) {
			try_again()
		}
	});
}


function button_on_click(element_id, call_back_function, params=null){
	$('#'+element_id).click(
		function(magic){
			return call_back_function(params)
	});
}


function initalize_buttons(){
	button_on_click('go', check_and_send);
}


function initalize(){
	initalize_buttons()
}


jQuery(document).ready(function() {
    jQuery(document).ready(function() {
    	initalize()
    });
})