rsvp_data = {}
total_rsvp_guests = 0
guests_breakdown = {}



function button_on_click(element_id, call_back_function, params=null){
	$('#'+element_id).click(
		function(magic){
			return call_back_function(params)
	});
}



function set_rsvp_data(data, total_guests, breakdown){
	rsvp_data = data
	total_rsvp_guests = total_guests
	guests_breakdown = breakdown
}


function assign_guest_of_color(guest_of, id){
	color = {
		'': '', 
		'AR': '#EBDEF0', 
		'AF': '#FADBD8', 
		'NR': '#D4E6F1', 
		'NF': '#D1F2EB'
	}[guest_of]
	$('#' + id).html(guest_of)
	$('#' + id).css({'background-color': color, 'cursor': 'pointer'})
}


function change_guest_of(id){
	$.ajax({
		type: 'POST',
		url: '/admin/change_guest_of',
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify(id),
		success: function(response) {
			assign_guest_of_color(response.next, id)
		},
		error: function(error) {}
	});
}


function initalize_table(){
	$('#rsvp_table').dataTable( {
		"columns": [
			null,
			{ "orderable": false },
			null,
			null,
			null,
			]
		} );
}


function build_table(){
	html = ''
	for (var rsvp in rsvp_data){
		guests = rsvp_data[rsvp]['guests']

		guests_html = '<td>'
		for (var i = 0; i < guests.length; i++){
			attend_class = 'glyphicon glyphicon-'

			if (guests[i][1] == true) {
				attend_class+='ok'
			}
			else{
				attend_class+='remove'
			}

			guests_html += '<div style="white-space: nowrap">' +
						   '	<span class="'+ attend_class +'"></span>&nbsp' +
						   '	<span>' + guests[i][0] + '</span>' +
						   '</div>'
		}

		guests_html += '</td>'

		html += '<tr>' +
				'	<td id="'+ rsvp_data[rsvp]['_id'] +'">' + rsvp_data[rsvp]['guest_of'] + '</td>' +
				'	<td>' + rsvp_data[rsvp]['leader'] + '</td>' + guests_html +
				'	<td>' + rsvp_data[rsvp]['comments'] + '</td>' +
				'	<td style="font-size:8px">' + rsvp_data[rsvp]['_id'] + '</td>' +
				'</tr>'
	}

	$("#rsvp_table_body").html(html)

	for (var rsvp in rsvp_data){
		button_on_click(rsvp_data[rsvp]['_id'], change_guest_of, rsvp_data[rsvp]['_id'])
		assign_guest_of_color(rsvp_data[rsvp]['guest_of'], rsvp_data[rsvp]['_id'])
	}
}


function set_total_rsvp_guests(){
	$("#total_guests").html(total_rsvp_guests)
	$("#ar").html('AR: '+ guests_breakdown['AR'])
	$("#af").html('AF: '+ guests_breakdown['AF'])
	$("#nr").html('NR: '+ guests_breakdown['NR'])
	$("#nf").html('NF: '+ guests_breakdown['NF'])
	$("#blank").html('TBD: '+ guests_breakdown[''])


}


function initalize(){
	build_table();
	initalize_table();
	set_total_rsvp_guests();
}


jQuery(document).ready(function() {
	jQuery(document).ready(function() {
		initalize()
	});
})