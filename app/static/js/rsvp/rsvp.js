var guest_count = 0;
var slide_speed = 500;
var max_guest = 7;


var data = {
	'leader': {
		'name': '',
		'email': '',
		'attendance': true
	},
	'guests': {},
	'comments': ''
};


// Helper Functions

function call_back_function(function_to_call, params){
	function_to_call(params)
}


function post_data(data, 
				   function_to_call_on_success=null){
	$.ajax({
		type: 'POST',
		url: '/rsvp/post_rsvp_data',
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify(data),
		success: function(response) {
			if (function_to_call_on_success){
				call_back_function(function_to_call_on_success,
							   response.data)				
			}
		},
		error: function(error) {}
	});
}


function button_on_click(element_id, call_back_function, params=null){
	$('#'+element_id).click(
		function(magic){
			return call_back_function(params)
	});
}


function input_insert_on_change(element_id, call_back_function, params=null){
	$('#'+element_id).keyup(
		function(magic){
			return call_back_function([magic, params])
	});
}


function add_class_to_element(element_id, css_class){
	$('#'+element_id).addClass(css_class)
		
}


function remove_class_from_element(element_id, css_class){
	$('#'+element_id).removeClass(css_class)
}


// RSVP Functions

function check_for_existing(){
	if (existing_data){
		console.log(existing_data)
	}
}


function insert_name(params){
	var magic = params[0];
	var person = params[1];
	var value = $('#'+magic.target.id).val()

	if (person == 'leader'){
		data[person]['name'] = value
	}
	else {
		data['guests'][person]['name'] = value
	}
}


function insert_comments(params){
	var magic = params[0];
	data['comments'] = $('#'+magic.target.id).val()
}


function insert_email(params){
	var magic = params[0];
	data['leader']['email'] = $('#'+magic.target.id).val()
}



function toggle_attendance(params){
	function change_class(selected_eid, unselected_eid){
		add_class_to_element(selected_eid, 'btn-selected')
		remove_class_from_element(unselected_eid, 'btn-selected')
	}

	var person = params[0];
	var action = params[1];

	if (person == 'leader'){
		data['leader']['attendance'] = action
		if (action) {
 			change_class('leader_attending_btn',
 						 'leader_not_attending_btn')
		}
		else if (!action) {
 			change_class('leader_not_attending_btn',
 						 'leader_attending_btn')
		}
		return
	}

	data['guests'][person]['attendance'] = action
	if (action) {
			change_class(person + '_attending_btn',
						 person + '_not_attending_btn')
	}
	else if (!action) {
			change_class(person + '_not_attending_btn',
						 person + '_attending_btn')
	}
}


function build_additional_guest_html(){
	function remove_guest(){
		$('#'+guest_id).slideUp(slide_speed, function(){
			$('#'+guest_id).remove();
			delete data["guests"][guest_id]
			if (Object.keys(data['guests']).length < max_guest){
				$('#add_guest_btn').slideDown(slide_speed)
			}
		})
	}

	guest_count++;

	var guest_id = "guest_" + guest_count

	html = $([
		'<div id="' + guest_id +'"',
		'     class="row guest_row">',
		'  <input id="' + guest_id +'_name_input"',
		'         type="text"',
		'         class="form-control name-input"',
		'         placeholder="Guest Full Name"',
		'         style="margin-bottom: 6px">',
		'',
		'  <button id="' + guest_id +'_attending_btn"',
		'          type="button"',
		'          class="btn btn-default btn-rsvp btn-selected"',
		'          style="margin-right: 5px">',
		'    Attending',
		'  </button>',
		'',
		'  <button id="' + guest_id +'_not_attending_btn"',
		'          type="button"',
		'          class="btn btn-default btn-rsvp">',
		'    Not Attending',
		'  </button>',
		'',
		'  <button id="' + guest_id +'_remove_btn"',
		'          type="button"',
		'          style="float:right"',
		'          class="btn btn-default btn-rsvp btn-remove">',
		'    remove',
		'  </button>',
		'</div>'
	].join("\n"));

	$("#additional_guests_area").append(html)

	data["guests"][guest_id] = {
		'name': '',
		'email': '',
		'attendance': true
	};

	$('#'+guest_id).slideDown(slide_speed)

	button_on_click(guest_id + "_remove_btn", remove_guest);
	button_on_click(guest_id + '_attending_btn',
					toggle_attendance,
					[guest_id, true])
	button_on_click(guest_id + '_not_attending_btn',
					toggle_attendance,
					[guest_id, false])

	input_insert_on_change(guest_id + '_name_input', 
						   insert_name, 
						   guest_id)

	if (Object.keys(data['guests']).length >= max_guest){
		$('#add_guest_btn').slideUp(slide_speed)
	}
}


function gather_rsvp_data(){
	results = post_data(data);
	add_class_to_element('rsvp-btn-main', 'hide')
	$('#thankyou_message').fadeIn(3000)
}


function initalize_inputs(){
	input_insert_on_change('leader_name_input', 
						   insert_name, 
						   'leader')
	input_insert_on_change('comments_input', insert_comments)
	input_insert_on_change('email_input', insert_email)
}




function initalize_buttons(){
	button_on_click('submit_rsvp_btn', gather_rsvp_data);
	button_on_click('add_guest_btn', build_additional_guest_html);
	button_on_click('leader_attending_btn',
					toggle_attendance,
					['leader', true])
	button_on_click('leader_not_attending_btn',
					toggle_attendance,
					['leader', false])

}


function initalize(){
	initalize_buttons()
	initalize_inputs()
	// check_for_existing()
}


jQuery(document).ready(function() {
    jQuery(document).ready(function() {
    	// document.getElementById("rsvp-btn-main").click();
    	initalize()
    	$('#thankyou_message').hide()
    });
})