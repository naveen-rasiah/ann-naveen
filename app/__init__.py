import sys

from flask import Flask
from flask_mongoalchemy import MongoAlchemy

app = Flask(__name__,
			instance_relative_config=True)

app.config.from_pyfile('config.py')
app.secret_key = 'yummy-secrets'

db = MongoAlchemy(app)


from save_the_date.views import save
from save_the_date.views import date
from rsvp.views import rsvp
from admin.views import admin
from invite.views import invite
from admin.views import admin
from bridal.views import bridal

app.register_blueprint(save)
app.register_blueprint(date)
app.register_blueprint(rsvp)
app.register_blueprint(admin)
app.register_blueprint(invite)
app.register_blueprint(bridal)